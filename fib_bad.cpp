#include <iostream>
#include <vector>

unsigned long long int fib(int n){
  if(n == 0 || n == 1){
    return n;
  }
  return fib(n - 1) + fib(n - 2);
}

int main(int argc, char * argv[]){
  for(unsigned long long int number = 1; number < argc; number++){
    std::cout << argv[number] << ": ";
    std::cout << fib(atoi(argv[number])) << std::endl;
  }
  std::cout << std::endl;
}
