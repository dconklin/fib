#include <iostream>
#include <climits>
#include <vector>

/*
 * Usage: ./a.out 0 10 3 93 .... X
 */

int main(int argc, char * argv[]){

  // Sets the two initial values for fibonacci sequence
  std::vector <unsigned long long int> nums;
  nums.push_back(0);
  nums.push_back(1);

  for(unsigned long long int num = 1; num < argc; num++){
    if(atoi(argv[num]) > 93) std::cout << "(Not accurate) ";
    std::cout << argv[num] << ": ";
    if(atoi(argv[num]) > nums.size()){
      do{
        nums.push_back(nums[nums.size() - 1] + nums[nums.size() - 2]);
      }while(nums.size() <= atoi(argv[num]));
    }
    std::cout << nums[atoi(argv[num])] << std::endl;
  }
  std::cout << std::endl;
}
